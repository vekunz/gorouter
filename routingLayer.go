package gorouter

import (
	"net/http"
)

type routingLayer struct {
	name string

	isParameter     bool
	isWildcardLayer bool
	hasChildRoutes  bool

	executor          executorMap
	preExecutor       executorMap
	postExecutor      executorMap
	groupPreExecutor  ExecutorFn
	groupPostExecutor ExecutorFn
	childRoutes       []routingLayer
}

func (rl *routingLayer) checkRelativeRoute(route string, req *http.Request) (matches bool, execCheck error, executor ExecutorFn, preExecutorStack []ExecutorFn, postExecutorStack []ExecutorFn, parameter map[string]string) {
	defer func() {
		if rl.groupPreExecutor != nil {
			preExecutorStack = append([]ExecutorFn{rl.groupPreExecutor}, preExecutorStack...)
		}
		if rl.groupPostExecutor != nil {
			postExecutorStack = append(postExecutorStack, rl.groupPostExecutor)
		}
	}()

	parameter = make(map[string]string)
	method := req.Method
	accept := req.Header.Get("Accept")

	last, head, subRoute := shiftPath(route)

	var layer *routingLayer
	found := false
	execCheck = nil
	errWrongMime := false
	for index := range rl.childRoutes {
		elem := &rl.childRoutes[index]
		_, err := elem.executor.GetExecutor(accept, method)
		execCheck = err
		if elem.isWildcardLayer && execCheck != errorMimeTypeNotAllowed {
			parameter["*"] = route
			found = true
			layer = elem
			last = true
			break
		} else if !elem.isParameter && elem.name == head {
			if !last {
				matches, execCheck, executor, preExecutorStack, postExecutorStack, parameter = elem.checkRelativeRoute(subRoute, req)
				if !matches {
					if execCheck == errorMimeTypeNotAllowed {
						errWrongMime = true
					}
					continue
				} else {
					return
				}
			} else {
				found = true
				layer = elem
				if err != errorMimeTypeNotAllowed {
					break
				} else {
					errWrongMime = true
					continue
				}
			}
		} else if elem.isParameter {
			if !last {
				matches, execCheck, executor, preExecutorStack, postExecutorStack, parameter = elem.checkRelativeRoute(subRoute, req)
				if !matches {
					continue
				} else {
					parameter[elem.name] = head
					return
				}
			} else {
				parameter[elem.name] = head
				found = true
				layer = elem
				break
			}
		}
	}

	if execCheck != nil && errWrongMime {
		execCheck = errorMimeTypeNotAllowed
	}

	if !found || (last && execCheck != nil) || layer == nil {
		return false, execCheck, nil, []ExecutorFn{}, []ExecutorFn{}, parameter
	}

	var err error
	if executor, err = layer.executor.GetExecutor(accept, method); err == nil {
		matches = true
		preExecutorStack = []ExecutorFn{}
		postExecutorStack = []ExecutorFn{}

		if layer.groupPreExecutor != nil {
			preExecutorStack = append([]ExecutorFn{layer.groupPreExecutor}, preExecutorStack...)
		}

		if exec, _ := layer.preExecutor.GetExecutor(accept, method); exec != nil {
			preExecutorStack = append([]ExecutorFn{exec}, preExecutorStack...)
		}
		if exec, _ := layer.postExecutor.GetExecutor(accept, method); exec != nil {
			preExecutorStack = append(postExecutorStack, exec)
		}
		if layer.groupPostExecutor != nil {
			postExecutorStack = append(postExecutorStack, layer.groupPostExecutor)
		}
		return
	}

	return false, execCheck, nil, []ExecutorFn{}, []ExecutorFn{}, parameter
}

func (rl *routingLayer) addChildRoute(route string, method string, accept string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn) {
	if route == "" {
		rl.executor.SetExecutor(accept, method, executor)
		rl.preExecutor.SetExecutor(accept, method, preExecutor)
		rl.postExecutor.SetExecutor(accept, method, postExecutor)
		return
	}
	last, head, subRoute := shiftPath(route)

	rl.hasChildRoutes = true

	childRoute := rl.getOrCreateChildRoute(head)
	if last {
		childRoute.executor.SetExecutor(accept, method, executor)
		childRoute.preExecutor.SetExecutor(accept, method, preExecutor)
		childRoute.postExecutor.SetExecutor(accept, method, postExecutor)
	} else {
		childRoute.addChildRoute(subRoute, method, accept, executor, preExecutor, postExecutor)
	}
}

func (rl *routingLayer) addRoutingLayer(layer *routingLayer) {
	childRoute := rl.getOrCreateChildRoute(layer.name)

	childRoute.name = layer.name

	childRoute.isParameter = layer.isParameter
	childRoute.isWildcardLayer = layer.isWildcardLayer
	childRoute.hasChildRoutes = layer.hasChildRoutes
	childRoute.preExecutor = layer.preExecutor
	childRoute.postExecutor = layer.postExecutor
	childRoute.groupPreExecutor = layer.groupPreExecutor
	childRoute.groupPostExecutor = layer.groupPostExecutor

	childRoute.executor = layer.executor
	childRoute.childRoutes = append(childRoute.childRoutes, layer.childRoutes...)
}

func (rl *routingLayer) getOrCreateChildRoute(childRouteName string) *routingLayer {
	isParameter := false
	if string([]rune(childRouteName)[0]) == ":" {
		childRouteName = string([]rune(childRouteName)[1:])
		isParameter = true
	}

	for index := range rl.childRoutes {
		elem := &rl.childRoutes[index]
		if elem.name == childRouteName {
			return elem
		}
	}
	route := routingLayer{
		name: childRouteName,

		isParameter:     isParameter,
		isWildcardLayer: childRouteName == "*",
		hasChildRoutes:  false,

		executor:     createExecutorMap(),
		preExecutor:  createExecutorMap(),
		postExecutor: createExecutorMap(),
		childRoutes:  []routingLayer{},
	}
	rl.childRoutes = append(rl.childRoutes, route)
	return &rl.childRoutes[len(rl.childRoutes)-1]
}
