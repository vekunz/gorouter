/*
GoRouter is an easy to use routing framework for the golang build in http server.

After creating a new router you can add routes to it and finally add it as handler to an http server

    var myrouter = gorouter.CreateRouter(nil, nil)
    ...
    var route = gorouter.NewRoute("hello", "GET", handlerFunc, nil, nil)
    myrouter.AddRoute(route)
    ...
    myrouter.NewRoute("foo/bar", "GET", handlerFunc2, nil, nil)
    ...
    server := &http.Server{
        Addr:    ":8080",
        Handler: myrouter,
    }
    server.ListenAndServe()

You can also create groups as one layer of the routes.

Every part, the router, the groups, the routes can have a preExecutor function and a postExecutor function.
These functions are a kind of middleware you can add to the router. The different pre- and postExecutor are
executed like these in the execution chain before and after the actual executor of the route. You can abort
this chain at any stage and send the http response back to the browser immediately.

    router:        preExecutor     postExecutor
                        |              / \
                       \ /              |
    group(s):      preExecutor     postExecutor
    (if present)        |              / \
                       \ /              |
    route:         preExecutor     postExecutor
                        |              / \
                       \ /              |
                             executor

A route can include placeholders. For example if a route shall contain an id, you can use a placeholder as
layer. A placeholder start every time with a : followed by any string naming the placeholder.

    /route/to/something/:placeholder

You can also use a wildcard as a routing layer. Such a route matches every route starting with the part before the
wildcard.

    /route/with/wildcard/'*'  <- (without the quotes) matches every route starting with '/route/with/wildcard/',
                                but not '/route/with/wildcard' itself

    /'*' <- (without the quotes) matches just every route

Note that every route added to the router after a wildcard route is added that is more specific than the wildcard
route is ignored. Make sure that you add all routes which are more specific than the wildcard route to the router
before you add the wildcard route.

*/
package gorouter
