package gorouter

import (
	"errors"
	"sort"
	"strconv"
	"strings"
)

// Represent one route of your http server. The 'Path' must not contain a leading slash. The 'Accept' header is the mime
// type for which this route/executor shall match. The mime type can either be <MIME_type>/<MIME_subtype> or <MIME_type>/*
// or */* or empty, q-Values are not allowed here, you can add multiple mime types comma separated.
type Route struct {
	Path         string
	Method       string
	Accept       string
	Executor     ExecutorFn
	PreExecutor  ExecutorFn
	PostExecutor ExecutorFn
}

var errorMimeTypeNotAllowed = errors.New("mime type not allowed")
var errorMethodNotAllowed = errors.New("method not allowed")
var errorNotFound = errors.New("not found")

func createExecutorMap() executorMap {
	return executorMap{
		executors: make(map[string]map[string]ExecutorFn),
	}
}

type executorMap struct {
	executors map[string]map[string]ExecutorFn
}

func (e *executorMap) SetExecutor(mimeType string, method string, executor ExecutorFn) {
	if executor == nil {
		return
	}
	for _, mime := range strings.Split(mimeType, ",") {
		mime = strings.ToLower(strings.TrimSpace(mime))
		if mime == "" {
			mime = "*/*"
		}
		if _, exists := e.executors[mime]; !exists {
			e.executors[mime] = make(map[string]ExecutorFn)
		}
		e.executors[mime][method] = executor
	}
}

func (e *executorMap) GetExecutor(mimeType string, method string) (ExecutorFn, error) {
	if len(e.executors) == 0 {
		return nil, errorNotFound
	}

	mimes := strings.Split(mimeType, ",")
	sort.SliceStable(mimes, func(i, j int) bool {
		iQ := e.getMimeQuality(mimes[i])
		jQ := e.getMimeQuality(mimes[j])
		return iQ > jQ
	})

	for _, mime := range mimes {
		var cMime string
		var err error
		if cMime, err = e.checkMime(mime, method); err != nil {
			continue
		}

		if _, exists := e.executors[cMime][method]; !exists {
			if _, exists := e.executors[cMime][""]; exists {
				method = ""
			} else {
				return nil, errorMethodNotAllowed
			}
		}
		return e.executors[cMime][method], nil
	}

	return nil, errorMimeTypeNotAllowed
}

func (e *executorMap) checkMime(mime string, method string) (string, error) {
	mime = strings.ToLower(strings.TrimSpace(strings.Split(mime, ";")[0]))
	if mime == "" {
		mime = "*/*"
	}

	parts := strings.Split(mime, "/")
	if len(parts) != 2 {
		return "", errors.New("invalid mime type")
	}
	mType := parts[0]
	mSubType := parts[1]

	if _, exists := e.executors[mType + "/" + mSubType]; exists {
		if _, exists2 := e.executors[mType + "/" + mSubType][method]; exists2 {
			return mType + "/" + mSubType, nil
		}
	}

	if _, exists := e.executors[mType + "/*"]; exists {
		if _, exists2 := e.executors[mType + "/*"][method]; exists2 {
			return mType + "/*", nil
		}
	}

	if _, exists := e.executors["*/*"]; exists {
		if _, exists2 := e.executors["*/*"][method]; exists2 {
			return "*/*", nil
		}
	}

	return "", errors.New("mime type not found")
}

func (e *executorMap) getMimeQuality(mime string) float64 {
	mime = strings.ToLower(strings.TrimSpace(mime))
	parts := strings.Split(mime, ";")
	if len(parts) == 1 {
		return 1.0
	}
	qValue := 1.0
	for _, param := range parts[1:] {
		prts := strings.Split(strings.TrimSpace(param), "=")
		if len(prts) != 2 || prts[0] != "q" {
			continue
		}
		var err error
		qValue, err = strconv.ParseFloat(strings.TrimSpace(prts[1]), 64)
		if err != nil {
			continue
		}
		break
	}
	return qValue
}