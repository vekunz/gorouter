package gorouter

import (
	"net/http"
	"path"
	"strings"
)

func shiftPath(p string) (last bool, head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return true, p[1:], "/"
	}
	return false, p[1:i], p[i:]
}

// This function which is used for every executor, perExecutor and postExecutor.
// The first two params are directly from the http server. The third param contains
// all placeholders of the current routes (in case of a preExecutor all placeholders
// currently available), it also includes the rest of an route if the route has a
// wildcard.
type ExecutorFn func(w http.ResponseWriter, r *http.Request, params map[string]string) (continueStack bool)

// This function creates a new router. It takes an optional preExecutor function and an
// optional postExecutor function
func CreateRouter(preExecutor ExecutorFn, postExecutor ExecutorFn) Router {
	return &router{
		masterRoute: routingLayer{
			name: "",

			isParameter:     false,
			isWildcardLayer: false,
			hasChildRoutes:  false,

			executor:          createExecutorMap(),
			preExecutor:       createExecutorMap(),
			postExecutor:      createExecutorMap(),
			groupPreExecutor:  preExecutor,
			groupPostExecutor: postExecutor,
			childRoutes:       []routingLayer{},
		},
	}
}

// This is a router itself. A router can only be created with the CreateRouter(..) function.
type Router interface {
	// Not important for the user, this function is only for the http server
	ServeHTTP(w http.ResponseWriter, r *http.Request)

	// Adds a route object to the router. Returns false if this fails
	AddRoute(rt Route)

	// Adds a routing group to the router. Returns false if this fails
	AddGroup(rg Group)

	// Shorthand for Route{..} and Router.AddRoute(..)
	NewRoute(path string, method string, accept string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn)

	// Shorthand for NewGroup(..) and Router.AddGroup(..)
	NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route)
}

type router struct {
	masterRoute routingLayer
}

func (r *router) ServeHTTP(wrt http.ResponseWriter, req *http.Request) {
	matches, execCheck, executor, preExecutorStack, postExecutorStack, parameter := r.masterRoute.checkRelativeRoute(req.URL.Path, req)

	if matches && executor != nil {
		for _, preExecutor := range preExecutorStack {
			if !preExecutor(wrt, req, parameter) {
				return
			}
		}
		if !executor(wrt, req, parameter) {
			return
		}
		for _, postExecutor := range postExecutorStack {
			if !postExecutor(wrt, req, parameter) {
				return
			}
		}

	} else {
		if r.masterRoute.groupPreExecutor != nil &&  !r.masterRoute.groupPreExecutor(wrt, req, parameter) {
			return
		}

		if matches && executor == nil {
			wrt.WriteHeader(501)
		} else if execCheck != nil {
			if execCheck == errorMethodNotAllowed {
				wrt.WriteHeader(405)
			} else if execCheck == errorMimeTypeNotAllowed {
				wrt.WriteHeader(406)
			} else if execCheck == errorNotFound {
				wrt.WriteHeader(404)
			} else {
				wrt.WriteHeader(500)
			}
		} else {
			wrt.WriteHeader(404)
		}

		if r.masterRoute.groupPostExecutor != nil &&  !r.masterRoute.groupPostExecutor(wrt, req, parameter) {
			return
		}
	}
}

func (r *router) AddRoute(rt Route) {
	r.masterRoute.addChildRoute(rt.Path, rt.Method, rt.Accept, rt.Executor, rt.PreExecutor, rt.PostExecutor)
}

func (r *router) AddGroup(rg Group) {
	r.masterRoute.addRoutingLayer(rg.routingLayer())
}

func (r *router) NewRoute(path string, method string, accept string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn) {
	r.AddRoute(Route{
		Path:         path,
		Method:       method,
		Accept:       accept,
		Executor:     executor,
		PreExecutor:  preExecutor,
		PostExecutor: postExecutor,
	})
}

func (r *router) NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route) {
	r.AddGroup(NewGroup(name, preExecutor, postExecutor, routes))
}
