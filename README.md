# gorouter

[![GoDoc](https://godoc.org/gitlab.com/vekunz/gorouter?status.svg)](https://godoc.org/gitlab.com/vekunz/gorouter)

v0.2

This README.md is generated with [github.com/robertkrimen/godocdown](https://github.com/robertkrimen/godocdown)

---

`import "gitlab.com/vekunz/gorouter"`

GoRouter is an easy to use routing framework for the golang build in http
server.

After creating a new router you can add routes to it and finally add it as
handler to an http server

    var myrouter = gorouter.CreateRouter(nil, nil)
    ...
    var route = gorouter.NewRoute("hello", "GET", handlerFunc, nil, nil)
    myrouter.AddRoute(route)
    ...
    myrouter.NewRoute("foo/bar", "GET", handlerFunc2, nil, nil)
    ...
    server := &http.Server{
        Addr:    ":8080",
        Handler: myrouter,
    }
    server.ListenAndServe()

You can also create groups as one layer of the routes.

Every part, the router, the groups, the routes can have a preExecutor function
and a postExecutor function. These functions are a kind of middleware you can
add to the router. The different pre- and postExecutor are executed like these
in the execution chain before and after the actual executor of the route. You
can abort this chain at any stage and send the http response back to the browser
immediately.

    router:        preExecutor     postExecutor
                        |              / \
                       \ /              |
    group(s):      preExecutor     postExecutor
    (if present)        |              / \
                       \ /              |
    route:         preExecutor     postExecutor
                        |              / \
                       \ /              |
                             executor

A route can include placeholders. For example if a route shall contain an id,
you can use a placeholder as layer. A placeholder start every time with a :
followed by any string naming the placeholder.

    /route/to/something/:placeholder

You can also use a wildcard as a routing layer. Such a route matches every route
starting with the part before the wildcard.

    /route/with/wildcard/'*'  <- (without the quotes) matches every route starting with '/route/with/wildcard/',
                                but not '/route/with/wildcard' itself

    /'*' <- (without the quotes) matches just every route

Note that every route added to the router after a wildcard route is added that
is more specific than the wildcard route is ignored. Make sure that you add all
routes which are more specific than the wildcard route to the router before you
add the wildcard route.

## Usage

#### type ExecutorFn

```go
type ExecutorFn func(w http.ResponseWriter, r *http.Request, params map[string]string) (continueStack bool)
```

This function which is used for every executor, perExecutor and postExecutor.
The first two params are directly from the http server. The third param contains
all placeholders of the current routes (in case of a preExecutor all
placeholders currently available), it also includes the rest of an route if the
route has a wildcard.

#### type Route

```go
type Route interface {
	// Returns the path of the route.
	Path() string

	// Returns the function that will be called if this route is called.
	Executor() ExecutorFn

	// Return the preExecutor of this route.
	PreExecutor() ExecutorFn

	// Return the postExecutor of this route.
	PostExecutor() ExecutorFn

	// Return the http method to which this route shall listen.
	Method() string
}
```

Represent one route of your http server. A new route can be created with the
NewRoute(..) functions.

#### func  NewRoute

```go
func NewRoute(path string, method string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn) Route
```
Creates a new route. The first parameter can either be just a name or a path.
The 'path' must not contain a leading slash. The second parameter is the http
method, for which this executor shall be. The third, fourth and fifth parameter
are the executor function, the preExecutor and the postExecutor.

#### type RouteGroup

```go
type RouteGroup interface {
	// The name of the group. For example the route /hello/foo has the two groups 'hello' and 'foo'
	Name() string

	// Add a route to the group
	AddRoute(route Route) bool

	// Add a complete group to the group
	AddGroup(routeGrp RouteGroup) bool

	// Shorthand for NewRoute(..) and RouteGroup.AddRoute(..)
	NewRoute(path string, method string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn)

	// Shorthand for NewGroup(..) and RouteGroup.AddGroup(..)
	NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route)
	// contains filtered or unexported methods
}
```

Represent one layer of an route. A new group can be created with the
NewGroup(..) functions. You can create such a group as shorthand to simplify the
route creation. Note that you cannot add more routes to a group after you added
a group to an other group or to a router. If you add a group to an other group
or router which already has a group with this name, the existing group is
overwritten.

#### func  NewGroup

```go
func NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route) RouteGroup
```
Creates a new group with the given name. The second and third parameter are the
optional pre- and postExecutor. The fourth parameter is an optional set of
routes.

#### type Router

```go
type Router interface {
	// Not important for the user, this function is only for the http server
	ServeHTTP(w http.ResponseWriter, r *http.Request)

	// Adds a route object to the router. Returns false if this fails
	AddRoute(rt Route) bool

	// Adds a routing group to the router. Returns false if this fails
	AddGroup(rg RouteGroup) bool

	// Shorthand for NewRoute(..) and Router.AddRoute(..)
	NewRoute(path string, method string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn)

	// Shorthand for NewGroup(..) and Router.AddGroup(..)
	NewGroup(name string, routes []Route, preExecutor ExecutorFn, postExecutor ExecutorFn)
}
```

This is a router itself. A router can only be created with the CreateRouter(..)
function.

#### func  CreateRouter

```go
func CreateRouter(preExecutor ExecutorFn, postExecutor ExecutorFn) Router
```
This function creates a new router. It takes an optional preExecutor function
and an optional postExecutor function

## License

This package is provided under the terms of the MIT License.
