package gorouter

// Represent one layer of an route. A new group can be created with the NewGroup(..) functions.
// You can create such a group as shorthand to simplify the route creation.
// Note that you cannot add more routes to a group after you added a group to an other group or to a router.
// If you add a group to an other group or router which already has a group with this name, the existing group
// is overwritten.
type Group interface {
	// The name of the group. For example the route /hello/foo has the two groups 'hello' and 'foo'
	Name() string

	// Add a route to the group
	AddRoute(route Route)

	// Add routes to the group
	AddRoutes(routes []Route)

	// Add a complete group to the group
	AddGroup(routeGrp Group)

	// Shorthand for &Route{..} and RouteGroup.AddRoute(..)
	NewRoute(path string, method string, accept string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn)

	// Shorthand for NewGroup(..) and RouteGroup.AddGroup(..)
	NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route)
	routingLayer() *routingLayer
}

// Creates a new group with the given name. The second and third parameter are the optional pre- and postExecutor.
// The fourth parameter is an optional set of routes.
func NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route) Group {
	isParameter := false
	if string([]rune(name)[0]) == ":" {
		name = string([]rune(name)[1:])
		isParameter = true
	}

	rg := routeGroup{
		masterRoute: &routingLayer{
			name: name,

			isParameter:     isParameter,
			isWildcardLayer: name == "*",
			hasChildRoutes:  false,

			executor:     createExecutorMap(),
			preExecutor:  createExecutorMap(),
			postExecutor: createExecutorMap(),
			childRoutes:  []routingLayer{},

			groupPreExecutor:  preExecutor,
			groupPostExecutor: postExecutor,
		},
	}

	rg.AddRoutes(routes)

	return &rg
}

type routeGroup struct {
	masterRoute *routingLayer
}

func (r *routeGroup) routingLayer() *routingLayer {
	return r.masterRoute
}

func (r *routeGroup) Name() string {
	return r.masterRoute.name
}

func (r *routeGroup) AddRoute(route Route) {
	r.masterRoute.addChildRoute(route.Path, route.Method, route.Accept, route.Executor, route.PreExecutor, route.PostExecutor)
}

func (r *routeGroup) AddRoutes(routes []Route) {
	for _, route := range routes {
		r.AddRoute(route)
	}
}

func (r *routeGroup) AddGroup(routeGrp Group) {
	r.masterRoute.addRoutingLayer(routeGrp.routingLayer())
}

func (r *routeGroup) NewRoute(path string, method string, accept string, executor ExecutorFn, preExecutor ExecutorFn, postExecutor ExecutorFn) {
	r.AddRoute(Route{
		Path:         path,
		Method:       method,
		Accept:       accept,
		Executor:     executor,
		PreExecutor:  preExecutor,
		PostExecutor: postExecutor,
	})
}

func (r *routeGroup) NewGroup(name string, preExecutor ExecutorFn, postExecutor ExecutorFn, routes []Route) {
	r.AddGroup(NewGroup(name, preExecutor, postExecutor, routes))
}
